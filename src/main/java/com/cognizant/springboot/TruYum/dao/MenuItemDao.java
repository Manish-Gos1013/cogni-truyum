package com.cognizant.springboot.TruYum.dao;
import java.util.List;

import com.cognizant.springboot.TruYum.model.MenuItem;


public interface MenuItemDao {
	public List<MenuItem> getMenuItemListAdmin();
	public List<MenuItem> getMenuItemListCustomer();
	public void  modifyMenuItem(MenuItemDao menuItem);
	public MenuItem getMenuItem(long menuItemId);
	void modifyMenuItem(MenuItem menuItem);
	public Object getDateOfLaunch(); 
}

