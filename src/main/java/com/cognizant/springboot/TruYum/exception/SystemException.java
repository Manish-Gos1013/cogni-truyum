package com.cognizant.springboot.TruYum.exception;
public class SystemException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public SystemException(String msg) {
		super(msg);
	}
}